# Assessment exercise 

## Your task
You are provided with a basic Drupal 8 installation as well as a MySQL database dump that contains some dummy content. 

There are two content types relevant for your task: event and course. An event contains a title, zero to many dates and zero to many references to the content type course. A course contains a title and a long text. 

Currently, when viewing a course, we can only see the title and body text of the course. Your task is to add a list of event titles with the corresponding date below the body text, similar to the mockup below. The list should be ordered by the event date in ascending order and dates in the past should not be displayed. Note that one event can contain multiple dates. In that case, the event title is repeated with each date. The date should be in the format dd.mm.yyyy. 

Example for what /node/1 should look like: 
![](mockup.png)

## Getting started
- [Fork](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html) this repository (you may have to create a BitBucket account first).
- Clone your forked repository and `cd` into it
- Run `composer install` to install dependencies
- Set up the database and import the dump `dimando_assessment.sql`
- You can log into Drupal with `superadmin` / `48BuK7D6X3mWHmb6`
- Implement your solution
- Run `drush config-export -y` to export the configuration
- Commit and push to your repository
- Send us the link to your repository
